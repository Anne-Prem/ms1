/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file <stats.h> 
 * @brief <it is to describe the functions used in stats.c >
 *
 *
 * @author <Prem Anne>
 * @date <5/9/2020 >
 *
 */
#ifndef __STATS_H__
#define __STATS_H__


/* Add Your Declarations and Function Comments here */
void print_statistics(unsigned char array[], unsigned int length);


/**
 *@brief<A function that prints the statistics of an array including minimum, maximum, mean, and median>.
 *   
 *@param <a> < The array containing the dataset of unsigned chars>
 *@param <length> < The length of the array>
 *       
*@return <NULL>
 */

void print_array(unsigned char array[], unsigned int length);


/** 
 * @brief< Given an array of data and a length, prints the array to the screen>
 * 
 *@param <a> < An array of unsigned chars>
 *@param <length> < The length of the array>
 *   
 *@return < NULL>
 */

unsigned char find_mean(unsigned char array[], unsigned int length);


/* 
 *@brief <Given an array of data and a length, returns the mean value>
 *     
 *@param <a> < An array of unsigned chars>
 *@param <length> < The length of the array>
 *        
 *@return <mean of the array>
 */


unsigned char find_median(unsigned char array[], unsigned int length);


/* 
 *@brief < Given an array of data and a length, returns the median  value>
 *  
 *@param <a> < An array of unsigned chars>
 *@param <length> <The length of the array>
 *  
 *@return <median of the array>
 */

unsigned char find_min(unsigned char array[], unsigned int length);


/* 
 *@brief <Given an array of data and a length, returns the min  value>
 *  
 *@param <a> < An array of unsigned chars>
 *@param <length> <The length of the array>
 *      
 *@return <The min of the array>
 */

unsigned char find_max(unsigned char array[], unsigned int length);


/* 
 *@brief <Given an array of data and a length, returns the max value>
 * 
 *@param < a> < An array of unsigned chars>
 *@param <length> < The length of the array>
 *      
 *@return <the max  of the array>
 */



void sort_array(unsigned char array[], unsigned int length);
/** 
 *@brief <Given an array of data and a length, prints the arranged array in descending order>
 *  
 *@param <a> An array of unsigned chars>
 *@param <length> The length of the array>
 *          
 *@return <null>
 */




#endif /* __STATS_H__ */
