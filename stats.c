/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file <stats.c> 
 * @brief <to find statistics of give data set such as mean,median,max,min and to sort array in descending order >
 * @author <Prem Anne>
 * @date <5/9/2021>
 *
 */



#include <stdio.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)
void print_statistics(unsigned char a[], unsigned int length);
void print_array(unsigned char a[], unsigned int length);
unsigned char find_median(unsigned char a[], unsigned int length);
unsigned char find_mean(unsigned char a[], unsigned int length);
unsigned char find_max(unsigned char a[], unsigned int length);
unsigned char find_min(unsigned char a[], unsigned int length);
void sort_array(unsigned char a[], unsigned int length);
void main() {

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
			       7,  87, 250, 230,  99,   3, 100,  90};
 /* Other Variable Declarations Go Here */
/* Statistics and Printing Functions Go Here */
print_statistics(test, SIZE);
  }

void print_statistics(unsigned char a[], unsigned int length){
printf("The original array:\n\n");
print_array(a, length);
sort_array(a, length);
printf("The Sorted array:\n");
print_array(a, length);
printf("The Median is: %d\n", find_median(a, length));
printf("The Mean is: %d\n", find_mean(a, length));
printf("The Max is: %d\n", find_max(a, length));
printf("The Min is: %d\n\n", find_min(a, length));	
return;
}

void print_array(unsigned char a[], unsigned int length){
for(int i = 0; i < length/8; i++){
for(int j = 0; j < length/5; j++){
printf("%3d  ", a[8*i+j]);
}
printf("\n");
}
			
printf("\n");	
return;
}

unsigned char find_median(unsigned char a[], unsigned int length){
		return a[(length-1)/2];
}


unsigned char find_mean(unsigned char a[], unsigned int length){
unsigned int sum = 0;
for(int i = 0; i < length; i++){
sum += a[i];	
}
return (unsigned char)( sum / length );
}

unsigned char find_max(unsigned char a[], unsigned int length){
		return a[length-1];
}


unsigned char find_min(unsigned char a[], unsigned int length){
		return a[0];
}

void sort_array(unsigned char a[], unsigned int length){
unsigned char tmp;
for(int i = 0; i < length; i++){
for(int j = i; j < length; j++){
if(a[j] < a[i]){
tmp = a[j];
a[j] = a[i];
a[i] = tmp;
																										}
}
}
return;
}


